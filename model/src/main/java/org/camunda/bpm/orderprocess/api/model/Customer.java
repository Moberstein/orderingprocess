package org.camunda.bpm.orderprocess.api.model;

import com.google.gson.annotations.SerializedName;

public class Customer {
    @SerializedName("firstname")
    public String FirstName;

    @SerializedName("lastname")
    public String LastName;

    @SerializedName("phone")
    public String Phone;

    @SerializedName("mail")
    public String Mail;

    @SerializedName("companyname")
    public String CompanyName;

    @SerializedName("customerid")
    public String CustomerId;

    @SerializedName("address_street")
    public String Street;

    @SerializedName("address_city")
    public String City;

    @SerializedName("address_postal")
    public String Postal;

    @SerializedName("address_country")
    public String Country;

    @SerializedName("discount")
    public int Discount;

    @SerializedName("class")
    public String Class;
}
