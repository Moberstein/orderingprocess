package org.camunda.bpm.orderprocess;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("Order Process App")
public class OrderProcess extends ServletProcessApplication {
    // empty implementation
}
