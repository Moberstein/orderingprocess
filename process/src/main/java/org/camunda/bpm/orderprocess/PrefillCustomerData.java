package org.camunda.bpm.orderprocess;

import com.google.gson.Gson;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.orderprocess.api.model.Customer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.logging.Logger;

public class PrefillCustomerData implements JavaDelegate {
    private final static Logger LOGGER = Logger.getLogger("ORDER_PROCESS");

    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Processing customer data request");
        String customerId = execution.getVariable("customer_id").toString();

        LOGGER.info("Searching for info of customer " + customerId);
        String customerUri = "http://localhost:8080/api/customer/" + customerId;

        // TODO: Make get request to api and get results
        // TODO: Maybe check if request was successful

        String customerResponse = "TODO: Get actual response";
        // Response is in json format and should look something like this:
        // [
        //  {"firstname": "Ivor", "lastname": "Roy", "phone": "(089) 82291484", ...},
        //  {"firstname": "Peter", "lastname": "Berger", "phone": "(068) 35968089", ...},
        //  ...
        // ]

        // Converts Json Response to a type of Customer
        Customer customer = new Gson().fromJson(customerResponse.toString(), Customer.class);

        // TODO: Set variables to use in process
        execution.setVariable("customer_firstname", customer.FirstName);
    }
}
