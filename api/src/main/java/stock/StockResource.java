package stock;

import com.google.gson.Gson;
import org.camunda.bpm.orderprocess.api.model.StockEntry;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

@Path("/stock")
public class StockResource {
    private StockEntry[] stockEntries;

    public StockResource() throws IOException {
        URL resource = this.getClass().getResource("/stockdata.json");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(resource.openStream(), Charset.forName("UTF-8")));

        StringBuilder stockentries = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null)
            stockentries.append(inputLine);
        in.close();

        stockEntries = new Gson().fromJson(stockentries.toString(), StockEntry[].class);
    }

    @GET
    @Produces("application/json")
    public Response get(){
        return Response.ok(new Gson().toJson(stockEntries)).build();
    }

    @GET
    @Path("/{productid}")
    @Produces("application/json")
    public Response get(@PathParam("productid") String productid){
        for (StockEntry stockEntry: stockEntries){
            if(stockEntry.ProductId.equals(productid)){
                return Response.ok(new Gson().toJson(stockEntry)).build();
            }
        }

        return Response.status(404).build();
    }
}
