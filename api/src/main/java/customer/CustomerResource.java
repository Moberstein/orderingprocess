package customer;

import com.google.gson.Gson;
import org.camunda.bpm.orderprocess.api.model.Customer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

@Path("/customer")
public class CustomerResource {
    private Customer[] customerList;

    public CustomerResource() throws IOException {
        URL resource = this.getClass().getResource("/customerdata.json");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(resource.openStream(), Charset.forName("UTF-8")));

        StringBuilder customerdata = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null)
            customerdata.append(inputLine);
        in.close();

        customerList = new Gson().fromJson(customerdata.toString(), Customer[].class);
    }

    @GET
    @Produces("application/json")
    public Response get(){
        return Response.ok(new Gson().toJson(customerList)).build();
    }

    @GET
    @Path("/{customerid}")
    @Produces("application/json")
    public Response get(@PathParam("customerid") String customerid){
        for (Customer customer: customerList){
            if(customer.CustomerId.equals(customerid)){
                return Response.ok(new Gson().toJson(customer)).build();
            }
        }

        return Response.status(404).build();
    }
}
